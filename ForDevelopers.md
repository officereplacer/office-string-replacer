## 準備

* Visual Studio 2019をインストール
* Inno Setupをインストール
* Visual Studio起動時Nugetライブラリの復元を求められた場合は復元する
* Microsoft Officeをインストール
  * Excel、Word、PowerPoint、Access
  * 各アプリの「オプション」 -> 「トラストセンター」 -> 「トラストセンターの設定」 -> 「マクロの設定」 -> 「開発者向けのマクロ設定」 -> 「VBAプロジェクト オブジェクト モデルへのアクセスを信頼する」にチェックを入れる 

## ソースコード構成

* OfficeStringReplacerプロジェクト
  * OfficeStringReplacerのUIを提供する（WPF）
* ShortcutStringReplacerプロジェクト
  * ShortcutStringReplacerのUIを提供する（WPF）
  * OfficeStringReplacerからショートカット機能のみを抜き出したもの
* OfficeStringReplacerCore
  * 検索、置換処理のビジネスロジック部分
    * *Processer系ソースコード
      * 検索、置換処理で共通の処理を抽象化したもの
      * ファイルオープン、VBAマクロオープン、各処理オブジェクトの取得などを行う。
      * 各オブジェクトの処理の実体はReplacerやSearcherにて実装する
    * *Replacer系ソースコード
      * 置換処理特有の処理
    * *Searcher系ソースコード
      * 検索処理特有の処理
* OfficeStringReplacerCoreTests
  * Core関係のユニットテスト
  * 現状では、Workingフォルダがテストのdllが存在するフォルダと一致していないと上手く動作しない点に注意

## インストーラー

### ビルド

make.batを実行することで、署名なしのインストーラーを作成可能です。

make_signed.batを実行することで、署名ありのインストーラーを作成可能です。
インストーラーだけでなく、実際の実行プログラムにも署名します。

以下の成果物がdestに作成されます。

* `OfficeStringReplacerSetup-{version}-{arch}-Office.exe`
* `ShortcutStringReplacerSetup-{version}-{arch}.exe`

## CI

タグをPushすると、CI/CDにて署名なしのインストーラーを作成します。
GitLabのプロジェクト->ビルド->アーティファクト->setupジョブ->artifacts.zipにインストーラーが作成されます。