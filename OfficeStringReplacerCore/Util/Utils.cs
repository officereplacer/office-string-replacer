﻿using OfficeStringReplacerCore.Definision;
using OfficeStringReplacerCore.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using NLog;
using System.Text.RegularExpressions;

namespace OfficeStringReplacerCore.Util
{
    public static class Utils
    {
        private static readonly Logger Logger = LogManager.GetLogger(nameof(Utils));

        public static string GetCoreVersion()
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            return asm.GetName().Version.ToString();
        }

        public static void SetProperty(object targetObj, string propertyName, object value)
        {
            var property = targetObj.GetType().GetProperty(propertyName);
            if (property is null)
            {
                throw new ArgumentException($"No such property: {propertyName}");
            }
            property.SetValue(targetObj, value);
        }

        public static string GetProperty(object targetObj, string propertyName)
        {
            var property = targetObj.GetType().GetProperty(propertyName);
            if (property is null)
            {
                throw new ArgumentException($"No such property: {propertyName}");
            }
            return property.GetValue(targetObj).ToString();
        }

        private static IEnumerable<DisposableCom<T>> EnamerateAsDisposableComWithItem<T>(dynamic targetCom)
        {
            var count = targetCom.Count;
            for (int i = 1; i <= count; i++)
            {
                yield return new DisposableCom<T>(targetCom.Item(i));
            }
        }

        public static IEnumerable<DisposableCom<T>> EnamerateAsDisposableCom<T>(dynamic targetCom)
        {
            var count = targetCom.Count;
            for (int i = 0; i < count; i++)
            {
                yield return new DisposableCom<T>(targetCom[i]);
            }
        }

        public static IEnumerable<DisposableCom<Microsoft.Vbe.Interop.VBProject>> EnamerateAsDisposableCom(Microsoft.Vbe.Interop.VBProjects targetCom)
        {
            return EnamerateAsDisposableComWithItem<Microsoft.Vbe.Interop.VBProject>(targetCom);
        }

        public static IEnumerable<DisposableCom<Microsoft.Vbe.Interop.VBComponent>> EnamerateAsDisposableCom(Microsoft.Vbe.Interop.VBComponents targetCom)
        {
            return EnamerateAsDisposableComWithItem<Microsoft.Vbe.Interop.VBComponent>(targetCom);
        }

        public static string GetUserDataDir()
        {
            string appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            return Path.Combine(appData, Environments.AppName);
        }

        public static string GetCommonDataDir()
        {
            string appData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            return Path.Combine(appData, Environments.AppName);
        }

        public static string GetCommonConfigFilePath()
        {
            return Path.Combine(GetCommonDataDir(), Consts.ConfigFileName);
        }

        public static string GetUserConfigFilePath()
        {
            return Path.Combine(GetUserDataDir(), Consts.ConfigFileName);
        }

        public static string GetCurrentLogFilePath()
        {
            return Path.Combine(GetUserDataDir(), Consts.CurrentLogFileName);
        }

        public static string GetSearchFailedPath()
        {
            return Path.Combine(GetUserDataDir(), Consts.SearchFailedFileName);
        }

        public static string GetReplaceFailedPath()
        {
            return Path.Combine(GetUserDataDir(), Consts.ReplaceFailedFileName);
        }

        public static List<string> GetFiles(string directoryPath, string matchPattern = "")
        {
            var directoryInfo = new DirectoryInfo(directoryPath);
            var fileList = new List<string>();
            foreach (var childDirInfo in directoryInfo.EnumerateDirectories("*"))
            {
                try
                {
                    foreach (var file in GetFiles(childDirInfo.FullName, matchPattern))
                    {
                        fileList.Add(file);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Warn(ex);
                }
            }
            foreach (var fileInfo in directoryInfo.EnumerateFiles("*"))
            {
                try
                {
                    var fullName = fileInfo.FullName;
                    if (string.IsNullOrEmpty(matchPattern))
                    {
                        fileList.Add(fullName);
                        continue;
                    }
                    if (fullName.ContainsWithRegex(matchPattern, RegexOptions.IgnoreCase))
                    {
                        fileList.Add(fileInfo.FullName);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Warn(ex);
                }
            }
            return fileList;
        }

        internal static string CreateUniqTempFilePath()
        {
            return Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
        }
    }
}
