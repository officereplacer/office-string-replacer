﻿using NLog;
using OfficeStringReplacerCore.Definision;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Util
{
    public class FileBackupper
    {
        private Logger Logger { get; }
        private string OutputBaseDirectory { get; }
        private bool CopyDirectory { get; }
        public string BackupDirectoryName { get; }

        public FileBackupper(string outputBaseDirectory, bool copyDirectory, DateTime? createTime = null)
        {
            OutputBaseDirectory = outputBaseDirectory.TrimEnd('\\') + "\\"; //Add trailing \
            CopyDirectory = copyDirectory;
            var createDate = createTime ?? DateTime.Now;
            BackupDirectoryName = $"{Consts.BackupFolderBase}.{createDate:yyyyMMddHHmmss}";
            Logger = LogManager.GetLogger(GetType().Name);
        }

        public void Backup(string inputPath)
        {
            if (string.IsNullOrEmpty(inputPath))
            {
                return;
            }
            Logger.Info($"Start backup: {inputPath}");
            string outputFolder;
            if (CopyDirectory)
            {
                var inputFolder = Path.GetDirectoryName(inputPath);
                var root = Path.GetPathRoot(inputFolder);
                var folderFromRoot = root.Length > 0 ?
                    inputFolder.Substring(root.Length) :
                    inputFolder;
                folderFromRoot = folderFromRoot.TrimStart('\\');
                outputFolder = Path.Combine(OutputBaseDirectory, BackupDirectoryName, folderFromRoot);
            }
            else
            {
                outputFolder = Path.Combine(OutputBaseDirectory, BackupDirectoryName);
            }
            Logger.Info($"Dest directory: {outputFolder}");
            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }
            var fileName = Path.GetFileName(inputPath);
            var outputPath = Path.Combine(outputFolder, fileName);
            File.Copy(inputPath, outputPath);
            Logger.Info($"Finish backup: {inputPath}");
        }
    }
}
