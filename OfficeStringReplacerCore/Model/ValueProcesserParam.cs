﻿using System;
using System.Text.RegularExpressions;

namespace OfficeStringReplacerCore.Model
{
    public class ValueProcesserParam
    {
        public string From { get; set; }

        public string To { get; set; }

        public bool IgnoreCase { get; set; }

        public StringComparison Comparison => IgnoreCase ? StringComparison.CurrentCultureIgnoreCase : StringComparison.CurrentCulture;

        public RegexOptions RegexOptions => IgnoreCase ? RegexOptions.IgnoreCase : RegexOptions.None;
    }
}
