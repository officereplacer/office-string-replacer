﻿using System.Collections.Generic;
using System.IO;

namespace OfficeStringReplacerCore.Model
{
    public class ProcesserParam
    {
        public ValueProcesserParam ValueProcesserParam { get; set; }

        public string TargetPath { get; set; }

        public string TargetFullPath => Path.GetFullPath(TargetPath);

        public List<string> ExcludeFilePathPatternList { get; set; } = new List<string>();

        public HashSet<string> XmlTagsToBeIgnored { get; set; } = new HashSet<string>
        {
            "mc:AlternateContent"
        };
    }
}
