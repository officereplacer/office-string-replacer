﻿using System;
using System.Runtime.InteropServices;

namespace OfficeStringReplacerCore.Model
{
    public class DisposableCom<T> : IDisposable
    {
        public T Value { get; }

        private readonly object _lockObject = new object();

        private readonly Action<T> _beforeDispose;

        public DisposableCom(T comObject, Action<T> beforeDispose = null)
        {
            Value = comObject;
            _beforeDispose = beforeDispose;
        }

        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            lock (_lockObject)
            {
                if (!disposedValue)
                {
                    _beforeDispose?.Invoke(Value);
                    Marshal.ReleaseComObject(Value);
                    disposedValue = true;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
    }
}
