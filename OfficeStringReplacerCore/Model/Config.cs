﻿using System.Collections.Generic;

namespace OfficeStringReplacerCore.Model
{
    public class Config
    {
        public string From { get; set; }

        public string To { get; set; }

        public bool IgnoreCase { get; set; }

        public HashSet<string> XmlTagsToBeIgnored { get; set; } = new HashSet<string>
        {
            "mc:AlternateContent"
        };

        public string BackupDirectory { get; set; }
    }
}
