﻿using System.Collections.Generic;
using System.Linq;

namespace OfficeStringReplacerCore.Model
{
    public enum ContextStatus
    {
        Unknown = 0,
        Starting = 1,
        Excecuting = 2,
        Success = 3,
        Fail = 4,
        Canceled = 5
    }

    public enum ContextSubStatus
    {
        Unknown = 0,
        NotProcessed = 1,
        Processed = 2,
    }

    public class Context
    {
        private object _lockObj = new object();

        private ContextStatus _status = ContextStatus.Unknown;

        public ContextStatus Status {
            get
            {
                lock (_lockObj)
                {
                    return _status;
                }
            }
            set {
                lock (_lockObj)
                {
                    _status = value;
                }
            } 
        }

        private ContextSubStatus _subStatus = ContextSubStatus.Unknown;

        public ContextSubStatus SubStatus
        {
            get
            {
                lock (_lockObj)
                {
                    return _subStatus;
                }
            }
            set
            {
                lock (_lockObj)
                {
                    _subStatus = value;
                }
            }
        }

        public bool SkipRest
        {
            get
            {
                lock (_lockObj)
                {
                    return Status == ContextStatus.Success || Status == ContextStatus.Fail;
                }
            }
        }

        public bool _isCancelRequested;

        public bool IsCancelRequested
        {
            get
            {
                lock (_lockObj)
                {
                    return _isCancelRequested;
                }
            }
            set
            {
                lock (_lockObj)
                {
                    _isCancelRequested = value;
                }
            }
        }

        private HashSet<string> _successFiles = new HashSet<string>();

        public void AddSuccessFiles(string value)
        {
            lock (_lockObj)
            {
                _successFiles.Add(value);
            }
        }

        public HashSet<string> SuccessFiles
        {
            get
            {
                lock (_lockObj)
                {
                    return _successFiles.Select(_ => _).ToHashSet();
                }
            }
        }

        /// <summary>
        /// Files that processed by ValueProcesser's process
        /// </summary>
        private HashSet<string> _porocessedFiles = new HashSet<string>();

        public void AddProcessedFiles(string value)
        {
            lock (_lockObj)
            {
                _porocessedFiles.Add(value);
            }
        }

        public HashSet<string> ProcessedFiles
        {
            get
            {
                lock (_lockObj)
                {
                    return _porocessedFiles.Select(_ => _).ToHashSet();
                }
            }
        }

        private HashSet<string> _failedFiles = new HashSet<string>();

        public void AddFailedFiles(string value)
        {
            lock (_lockObj)
            {
                _failedFiles.Add(value);
            }
        }

        public HashSet<string> FailedFiles
        {
            get
            {
                lock (_lockObj)
                {
                    return _failedFiles.Select(_ => _).ToHashSet();
                }
            }
        }

        private string _processingFile;

        public string ProcessingFile
        {
            get
            {
                lock (_lockObj)
                {
                    return _processingFile;
                }
            }
            set
            {
                lock (_lockObj)
                {
                    _processingFile = value;
                }
            }
        }
    }
}
