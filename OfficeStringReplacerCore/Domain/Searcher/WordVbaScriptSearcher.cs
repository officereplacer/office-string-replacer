﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Searcher
{
    public class WordVbaScriptSearcher: WordVbaScriptProcesser
    {
        public WordVbaScriptSearcher(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueSearcher(param.ValueProcesserParam);
        }
    }
}
