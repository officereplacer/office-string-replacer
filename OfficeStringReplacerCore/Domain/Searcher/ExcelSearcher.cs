﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Searcher
{
    public class ExcelSearcher: ExcelProcesser
    {
        public ExcelSearcher(Context context, ProcesserParam param) : base(context, param)
        {
            XmlFileProcesser = new XmlFileSearcher(context, param);
            VbaScriptProcesser = new ExcelVbaScriptSearcher(context, param);
        }
    }
}
