﻿using Microsoft.Office.Interop.Access;
using Microsoft.Office.Interop.Access.Dao;
using Microsoft.Vbe.Interop;
using NLog;
using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OfficeStringReplacerCore.Domain.Searcher
{
    public class ValueSearcher : IValueProcesser
    {
        ValueProcesserParam Param { get; }
        private Logger Logger { get; }

        public ValueSearcher(ValueProcesserParam param)
        {
            Param = param;
            Logger = LogManager.GetLogger(GetType().Name);
        }

        public void Process(Context context, Recordset recordSet, Field field)
        {
            if (!(field.Value is string value))
            {
                return;
            }
            if (value.Contains(Param.From, Param.Comparison))
            {
                context.Status = ContextStatus.Success;
                context.SubStatus = ContextSubStatus.Processed;
                return;
            }
        }

        public void Process(Context context, object obj, string propertyName)
        {
            var type = obj.GetType();
            string currentValue = type.InvokeMember(propertyName, System.Reflection.BindingFlags.GetProperty, null, obj, null) as string;
            if (currentValue.Contains(Param.From, Param.Comparison))
            {
                context.Status = ContextStatus.Success;
                context.SubStatus = ContextSubStatus.Processed;
                return;
            }
        }

        public void ProcessTables(Context context, Database db)
        {
            using (var tableDefs = new DisposableCom<TableDefs>(db.TableDefs))
            {
                foreach (var tableDef in Utils.EnamerateAsDisposableCom<TableDef>(tableDefs.Value))
                {
                    using (tableDef)
                    {
                        var tableName = tableDef.Value.Name;
                        Logger.Debug($"Table name: {tableName}");
                        if (tableName.StartsWith("MSys"))
                        {
                            continue;
                        }
                        ProcessTable(context, db, tableDef.Value);
                        if (context.SkipRest)
                        {
                            return;
                        }
                    }
                }
            }
        }

        private bool ContainsInConnect(string connect)
        {
            foreach (var keyValue in connect.Split(';'))
            {
                var index = keyValue.IndexOf("=");
                if (index == -1)
                {
                    //Key name or preserved value
                    continue;
                }
                var value = keyValue.Substring(index + 1);
                if (value.Contains(Param.From, Param.Comparison))
                {
                    return true;
                }
            }
            return false;
        }

        public void ProcessTable(Context context, Database db, TableDef tableDef)
        {
            try
            {
                Logger.Debug($"Start to search name and connect.");
                Process(context, tableDef, nameof(tableDef.Name));
                if (context.SkipRest)
                {
                    return;
                }
                Process(context, tableDef, nameof(tableDef.SourceTableName));
                if (context.SkipRest)
                {
                    return;
                }
                if (ContainsInConnect(tableDef.Connect))
                {
                    context.Status = ContextStatus.Success;
                    context.SubStatus = ContextSubStatus.Processed;
                }
                if (context.SkipRest)
                {
                    return;
                }
            }
            finally
            {
                Logger.Debug($"Finish to search name and connect.");
            }
            if (!string.IsNullOrWhiteSpace(tableDef.Connect))
            {
                //Skip if the table is a link table;
                return;
            }
            Logger.Debug($"Start to search table values.");
            try
            {
                HashSet<string> targetColumns = new HashSet<string>();
                Logger.Debug($"Get target type columns");
                using (var fields = new DisposableCom<Fields>(tableDef.Fields))
                {
                    foreach (var field in Utils.EnamerateAsDisposableCom<Field>(fields.Value))
                    {
                        using (field)
                        {
                            switch (field.Value.Type)
                            {
                                case (short)DataTypeEnum.dbComplexText:
                                case (short)DataTypeEnum.dbText:
                                case (short)DataTypeEnum.dbMemo:
                                case (short)DataTypeEnum.dbChar:
                                    targetColumns.Add(field.Value.Name);
                                    break;
                            }
                        }
                    }
                }
                //If there is a lot of cloumns, excuting SQL fails because of 
                //exceeding parameter number limit.
                //To avoid that failure, excuting SQL by each columns.
                foreach (string targetColumn in targetColumns)
                {
                    Logger.Debug($"Search value of column: {targetColumn}");
                    var whereClause = "";
                    if (targetColumns.Count > 0)
                    {
                        whereClause = $" WHERE [{targetColumn}] LIKE '*{Param.From.EscapeToAccessLike()}*'";
                    }

                    using (var recordSet = new DisposableCom<Recordset>(db.OpenRecordset($"SELECT * FROM [{tableDef.Name}] {whereClause}")))
                    {
                        try
                        {
                            if (recordSet.Value.RecordCount == 0)
                            {
                                continue;
                            }
                            context.Status = ContextStatus.Success;
                            context.SubStatus = ContextSubStatus.Processed;
                            return;
                        }
                        finally
                        {
                            recordSet.Value.Close();
                        }
                    }
                }

                using (var fields = new DisposableCom<Fields>(tableDef.Fields))
                {
                    Logger.Debug($"Search column name.");
                    foreach (var field in Utils.EnamerateAsDisposableCom<Field>(fields.Value))
                    {
                        using (field)
                        {
                            Process(context, field.Value, nameof(field.Value.Name));
                            if (context.SkipRest)
                            {
                                return;
                            }
                        }
                    }
                }
            }
            finally
            {
                Logger.Debug($"Finish to search table values.");
            }
        }

        public void Process(Context context, CodeModule codeModule)
        {
            var lineCount = codeModule.CountOfLines;
            if (lineCount <= 0)
            {
                return;
            }
            var code = codeModule.Lines[1, lineCount];
            if (code.Contains(Param.From, Param.Comparison))
            {
                context.Status = ContextStatus.Success;
                context.SubStatus = ContextSubStatus.Processed;
                return;
            }
        }

        public void Process(Context context, DoCmd doCmd, AcObjectType acObjectType, string name)
        {
            if (name.Contains(Param.From, Param.Comparison))
            {
                context.Status = ContextStatus.Success;
                context.SubStatus = ContextSubStatus.Processed;
                return;
            }
        }
    }
}
