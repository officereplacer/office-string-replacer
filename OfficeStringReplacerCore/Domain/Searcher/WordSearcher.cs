﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Searcher
{
    public class WordSearcher: WordProcesser
    {
        public WordSearcher(Context context, ProcesserParam param) : base(context, param)
        {
            XmlFileProcesser = new XmlFileSearcher(context, param);
            VbaScriptProcesser = new WordVbaScriptSearcher(context, param);
        }
    }
}
