﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Searcher
{
    public class AccessSearcher : AccessProcesser
    {
        public AccessSearcher(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueSearcher(param.ValueProcesserParam);
            VbaScriptProcesser = new AccessVbaScriptSearcher(context, param);
        }

        protected override void PreProcess(string sourceFolderPath)
        {
        }

        protected override void PostProcess(string sourceFolderPath, string destFilePath)
        {
        }
    }
}