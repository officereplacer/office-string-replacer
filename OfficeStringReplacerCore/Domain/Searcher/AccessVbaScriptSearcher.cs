﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Searcher
{
    public class AccessVbaScriptSearcher : AccessVbaScriptProcesser
    {
        public AccessVbaScriptSearcher(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueSearcher(param.ValueProcesserParam);
        }
    }
}
