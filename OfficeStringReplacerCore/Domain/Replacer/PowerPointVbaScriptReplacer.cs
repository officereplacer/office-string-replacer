﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class PowerPointVbaScriptReplacer : PowerPointVbaScriptProcesser
    {
        public PowerPointVbaScriptReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueReplacer(param.ValueProcesserParam);
        }
    }
}
