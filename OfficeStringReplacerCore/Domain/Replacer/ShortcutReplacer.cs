﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class ShortcutReplacer : ShortcutProcesser
    {
        public ShortcutReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueReplacer(param.ValueProcesserParam);
        }

        protected override void PreProcess(string sourceFolderPath)
        {
        }

        protected override void PostProcess(string sourceFolderPath, string destFilePath)
        {
            if (File.Exists(destFilePath))
            {
                File.Delete(destFilePath);
            }
            File.Move(sourceFolderPath, destFilePath);
        }
    }
}
