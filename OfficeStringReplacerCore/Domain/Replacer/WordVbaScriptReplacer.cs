﻿using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class WordVbaScriptReplacer : WordVbaScriptProcesser
    {
        public WordVbaScriptReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            ValueProcesser = new ValueReplacer(param.ValueProcesserParam);
        }
    }
}
