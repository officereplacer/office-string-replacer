﻿using Microsoft.Office.Interop.Access;
using Microsoft.Office.Interop.Access.Dao;
using Microsoft.Vbe.Interop;
using NLog;
using OfficeStringReplacerCore.Definision;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public class ValueReplacer : IValueProcesser
    {
        ValueProcesserParam Param { get; }
        private Logger Logger { get; }

        public ValueReplacer(ValueProcesserParam param)
        {
            Param = param;
            Logger = LogManager.GetLogger(GetType().Name);
        }

        public void Process(Context context, Recordset recordSet, Field field)
        {
            if (!(field.Value is string value))
            {
                return;
            }
            if (!value.Contains(Param.From, Param.Comparison))
            {
                return;
            }
            recordSet.Edit();
            field.Value = value.ReplaceWithRegex(Regex.Escape(Param.From), Param.To, Param.RegexOptions);
            recordSet.Update();
            context.SubStatus = ContextSubStatus.Processed;
        }

        public void Process(Context context, object obj, string propertyName)
        {
            var type = obj.GetType();
            string currentValue = type.InvokeMember(propertyName, System.Reflection.BindingFlags.GetProperty, null, obj, null) as string;
            if (!currentValue.Contains(Param.From, Param.Comparison))
            {
                return;
            }
            var replacedValue = currentValue.ReplaceWithRegex(Regex.Escape(Param.From), Param.To, Param.RegexOptions);
            type.InvokeMember(propertyName, System.Reflection.BindingFlags.SetProperty, null, obj, new[] { replacedValue });
            context.SubStatus = ContextSubStatus.Processed;
        }

        private string GetNewConnect(string connect)
        {
            var newkeyValues = new HashSet<string>();
            foreach (var keyValue in connect.Split(';'))
            {
                var index = keyValue.IndexOf("=");
                if (index == -1)
                {
                    //Key name or preserved value
                    newkeyValues.Add(keyValue);
                    continue;
                }
                var key = keyValue.Substring(0, index);
                var value = keyValue.Substring(index + 1);
                if (value.Contains(Param.From, Param.Comparison))
                {
                    value = value.ReplaceWithRegex(Regex.Escape(Param.From), Param.To, Param.RegexOptions);
                }
                newkeyValues.Add($"{key}={value}");
            }
            return string.Join(";", newkeyValues);
        }

        private bool ContainsInConnect(string connect)
        {
            foreach (var keyValue in connect.Split(';'))
            {
                var index = keyValue.IndexOf("=");
                if (index == -1)
                {
                    //Key name or preserved value
                    continue;
                }
                var value = keyValue.Substring(index + 1);
                if (value.Contains(Param.From, Param.Comparison))
                {
                    return true;
                }
            }
            return false;
        }

        public void ProcessTables(Context context, Database db)
        {
            using (var tableDefs = new DisposableCom<TableDefs>(db.TableDefs))
            {
                var newTableDefArgs = new HashSet<(string, TableDefAttributeEnum, string, string)>();
                foreach (var tableDef in Utils.EnamerateAsDisposableCom<TableDef>(tableDefs.Value))
                {
                    using (tableDef)
                    {
                        var tableName = tableDef.Value.Name;
                        Logger.Debug($"Table name: {tableName}");
                        if (tableName.StartsWith("MSys"))
                        {
                            continue;
                        }
                        ProcessTable(context, db, tableDef.Value);
                        if (context.SkipRest)
                        {
                            return;
                        }

                        if (tableDef.Value.SourceTableName.Contains(Param.From, Param.Comparison))
                        {
                            var newSourceTable = tableDef.Value.SourceTableName.ReplaceWithRegex(Regex.Escape(Param.From), Param.To, Param.RegexOptions);
                            var newConnect = GetNewConnect(tableDef.Value.Connect);
                            newTableDefArgs.Add((tableDef.Value.Name, (TableDefAttributeEnum)tableDef.Value.Attributes, newSourceTable, newConnect));
                        }
                        else if (ContainsInConnect(tableDef.Value.Connect))
                        {
                            tableDef.Value.Connect = GetNewConnect(tableDef.Value.Connect);
                            tableDef.Value.RefreshLink();
                            context.SubStatus = ContextSubStatus.Processed;
                        }
                    }
                }

                foreach (var newTableDefArg in newTableDefArgs)
                {
                    var (name, attributes, sourceTable, connect) = newTableDefArg;
                    tableDefs.Value.Delete(name);
                    using (var newDef = new DisposableCom<TableDef>(db.CreateTableDef(name)))
                    {
                        newDef.Value.Connect = connect;
                        newDef.Value.SourceTableName = sourceTable;
                        if (attributes.HasFlag(TableDefAttributeEnum.dbAttachExclusive))
                        {
                            newDef.Value.Attributes |= (int)TableDefAttributeEnum.dbAttachExclusive;
                        }
                        if (attributes.HasFlag(TableDefAttributeEnum.dbAttachSavePWD))
                        {
                            newDef.Value.Attributes |= (int)TableDefAttributeEnum.dbAttachSavePWD;
                        }
                        if (attributes.HasFlag(TableDefAttributeEnum.dbHiddenObject))
                        {
                            newDef.Value.Attributes |= (int)TableDefAttributeEnum.dbHiddenObject;
                        }
                        try
                        {
                            tableDefs.Value.Append(newDef.Value);
                        }
                        catch (Exception ex) when ((uint)ex.HResult == Consts.DsnNotFoundErrorCode)
                        {
                            Logger.Warn(ex);
                            Logger.Warn("DSN not found, try DNS-less connection.");
                            //DSN not found, try DSN-less connecttion;
                            var newConnect = string.Join(";", connect.Split(';').Where(_ => !_.StartsWith("DSN")));
                            newDef.Value.Connect = newConnect;
                            tableDefs.Value.Append(newDef.Value);
                        }
                    }
                    context.SubStatus = ContextSubStatus.Processed;
                }
            }
        }


        private void ProcessTable(Context context, Database db, TableDef tableDef)
        {
            Process(context, tableDef, nameof(tableDef.Name));
            if (!string.IsNullOrWhiteSpace(tableDef.Connect))
            {
                //Skip if the table is a link table;
                return;
            }
            HashSet<string> targetColumns = new HashSet<string>();
            using (var fields = new DisposableCom<Fields>(tableDef.Fields))
            {
                foreach (var field in Utils.EnamerateAsDisposableCom<Field>(fields.Value))
                {
                    using (field)
                    {
                        switch (field.Value.Type)
                        {
                            case (short)DataTypeEnum.dbComplexText:
                            case (short)DataTypeEnum.dbText:
                            case (short)DataTypeEnum.dbMemo:
                            case (short)DataTypeEnum.dbChar:
                                targetColumns.Add(field.Value.Name);
                                break;
                        }
                    }
                }
            }

            foreach (string targetColumn in targetColumns)
            {
                var whereClause = "";
                if (targetColumns.Count > 0)
                {
                    whereClause = $" WHERE [{targetColumn}] LIKE '*{Param.From.EscapeToAccessLike()}*'";
                }

                using (var recordSet = new DisposableCom<Recordset>(db.OpenRecordset($"SELECT * FROM [{tableDef.Name}] {whereClause}")))
                {
                    try
                    {
                        if (recordSet.Value.RecordCount == 0)
                        {
                            continue;
                        }
                        db.Execute($@"UPDATE [{tableDef.Name}] 
                                SET [{targetColumn}] = REPLACE([{targetColumn}], '{Param.From.Replace("'", "''")}', '{Param.To.Replace("'", "''")}')
                                {whereClause}");
                        context.SubStatus = ContextSubStatus.Processed;
                    }
                    finally
                    {
                        recordSet.Value.Close();
                    }
                }
            }

            using (var fields = new DisposableCom<Fields>(tableDef.Fields))
            {
                foreach (var field in Utils.EnamerateAsDisposableCom<Field>(fields.Value))
                {
                    using (field)
                    {
                        Process(context, field.Value, nameof(field.Value.Name));
                    }
                }
            }
        }

        public void Process(Context context, CodeModule codeModule)
        {
            var lineCount = codeModule.CountOfLines;
            for (int i = 1; i <= lineCount; i++)
            {
                var line = codeModule.Lines[i, 1];
                if (!line.Contains(Param.From, Param.Comparison))
                {
                    continue;
                }
                var replacedLine = line.ReplaceWithRegex(Regex.Escape(Param.From), Param.To, Param.RegexOptions);
                codeModule.ReplaceLine(i, replacedLine);
                context.SubStatus = ContextSubStatus.Processed;
            }
        }

        public void Process(Context context, DoCmd doCmd, AcObjectType acObjectType, string name)
        {
            if (!name.Contains(Param.From, Param.Comparison))
            {
                return;
            }
            doCmd.Rename(
                name.ReplaceWithRegex(Regex.Escape(Param.From), Param.To, Param.RegexOptions),
                acObjectType,
                name);
            context.SubStatus = ContextSubStatus.Processed;
        }
    }
}
