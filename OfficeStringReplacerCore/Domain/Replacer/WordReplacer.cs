﻿using Ionic.Zip;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Word;
using OfficeStringReplacerCore.Domain.Processer;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System.IO;

namespace OfficeStringReplacerCore.Domain.Replacer
{
    public class WordReplacer : WordProcesser
    {
        public WordReplacer(Context context, ProcesserParam param) : base(context, param)
        {
            XmlFileProcesser = new XmlFileReplacer(context, param);
            VbaScriptProcesser = new WordVbaScriptReplacer(context, param);
        }

        private void CompressAsWordFile(string sourceFolderPath, string destFilePath)
        {
            var tmpFilePath = Utils.CreateUniqTempFilePath();
            try
            {
                using (ZipFile zipFileForDestFilePath = new ZipFile())
                {
                    zipFileForDestFilePath.AddDirectory(sourceFolderPath, "");
                    zipFileForDestFilePath.Save(tmpFilePath);
                }
                var ext = Path.GetExtension(destFilePath);
                switch (ext)
                {
                    case ".doc":
                        using (var wordApp = new DisposableCom<Microsoft.Office.Interop.Word.Application>(new Microsoft.Office.Interop.Word.Application() { 
                            DisplayAlerts = WdAlertLevel.wdAlertsMessageBox
                        }))
                        using (var documents = new DisposableCom<Documents>(wordApp.Value.Documents))
                        using (var document = new DisposableCom<Document>(documents.Value.Open(tmpFilePath)))
                        {
                            try
                            {
                                if (File.Exists(destFilePath))
                                {
                                    File.Delete(destFilePath);
                                }
                                document.Value.SaveAs(destFilePath, WdSaveFormat.wdFormatDocument);
                            }
                            finally
                            {
                                document.Value.Close(false);
                                wordApp.Value.Quit();
                            }
                        }
                        break;
                    default:
                        if (File.Exists(destFilePath))
                        {
                            File.Delete(destFilePath);
                        }
                        File.Move(tmpFilePath, destFilePath);
                        break;
                }
            }
            finally
            {
                if (File.Exists(tmpFilePath))
                {
                    File.Delete(tmpFilePath);
                }
            }
        }

        protected override void PostProcess(string sourceFolderPath, string destFilePath)
        {
            if (Context.Status == ContextStatus.Fail)
            {
                return;
            }
            CompressAsWordFile(sourceFolderPath, destFilePath);
        }
    }
}
