﻿using Ionic.Zip;
using OfficeStringReplacerCore.Infrastructure;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class XmlOfficeProcesser : Processer
    {
        protected XmlFileProcesser XmlFileProcesser { get; set; }

        protected VbaScriptProcesser VbaScriptProcesser { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="from">Replace from</param>
        /// <param name="to">Replace to</param>
        public XmlOfficeProcesser(Context context, ProcesserParam param) : base(context, param)
        {
        }

        public abstract bool IsTarget(string path);

        protected abstract bool IsMacroEnabled(string path);

        protected abstract string CopyAsTempOpenXMLFile(string sourceFilePath, bool isMacroEnabled);

        public void Process()
        {
            Context.Status = ContextStatus.Excecuting;
            var fullPath = ProcesserParam.TargetFullPath;
            try
            {
                if (ProcesserParam.ExcludeFilePathPatternList.Any(_ => Regex.IsMatch(fullPath, _, RegexOptions.IgnoreCase)))
                {
                    Context.Status = ContextStatus.Success;
                    return;
                }

                if (Directory.Exists(fullPath))
                {
                    var files = Utils.GetFiles(fullPath);
                    foreach (var file in files)
                    {
                        Process(file);
                        ThrowIfCancelRequested();
                        //Search all files
                        Context.Status = ContextStatus.Excecuting;
                        Context.SubStatus = ContextSubStatus.NotProcessed;
                    }
                }
                else if (File.Exists(fullPath))
                {
                    Process(fullPath);
                }
                if (Context.FailedFiles.Any())
                {
                    Context.Status = ContextStatus.Fail;
                }
                else
                {
                    Context.Status = ContextStatus.Success;
                }
            }
            catch (CancelException ex)
            {
                Context.Status = ContextStatus.Canceled;
                Logger.Info($"Canceled: {ex}");
                throw;
            }
            catch (Exception ex)
            {
                Context.Status = ContextStatus.Fail;
                Logger.Error(ex);
            }
        }

        public void Process(string filePath)
        {
            Context.Status = ContextStatus.Excecuting;
            Context.SubStatus = ContextSubStatus.NotProcessed;
            Context.ProcessingFile = filePath;
            var fullFilePath = Path.GetFullPath(filePath);
            if (!IsTarget(fullFilePath))
            {
                Context.Status = ContextStatus.Success;
                Context.AddSuccessFiles(fullFilePath);
                return;
            }
            var fileName = Path.GetFileName(fullFilePath);
            if (fileName.StartsWith("~$"))
            {
                Context.Status = ContextStatus.Success;
                Context.AddSuccessFiles(fullFilePath);
                return;
            }
            if (ProcesserParam.ExcludeFilePathPatternList.Any(_ => Regex.IsMatch(fullFilePath, _, RegexOptions.IgnoreCase)))
            {
                Context.Status = ContextStatus.Success;
                return;
            }
            Logger.Info($"Start processing: {filePath}");
            var isMacroEnabled = IsMacroEnabled(fullFilePath);
            var tmpFolderPath = Utils.CreateUniqTempFilePath();
            string tmpFilePath = "";
            try
            {
                tmpFilePath = CopyAsTempOpenXMLFile(fullFilePath, isMacroEnabled);
                PreProcess(tmpFilePath);
                if (isMacroEnabled)
                {
                    VbaScriptProcesser.Process(tmpFilePath);
                    if (Context.SkipRest)
                    {
                        return;
                    }
                }

                using (ZipFile zipFileForTmpFilePath = new ZipFile(tmpFilePath))
                {
                    Directory.CreateDirectory(tmpFolderPath);
                    zipFileForTmpFilePath.ExtractAll(tmpFolderPath);
                }
                var targetFiles = Utils.GetFiles(tmpFolderPath, @"\.(xml|rels)$");
                foreach (var targetFile in targetFiles)
                {
                    XmlFileProcesser.Process(targetFile);
                    if (Context.SkipRest)
                    {
                        return;
                    }
                }
                PostProcess(tmpFolderPath, fullFilePath);
                Context.Status = ContextStatus.Success;
            }
            catch (CancelException ex)
            {
                Context.Status = ContextStatus.Canceled;
                Logger.Info($"Canceled: {ex}");
                throw;
            }
            catch (Exception ex)
            {
                Context.Status = ContextStatus.Fail;
                Logger.Error(ex);
            }
            finally
            {
                switch (Context.Status)
                {
                    case ContextStatus.Success:
                        Context.AddSuccessFiles(fullFilePath);
                        switch (Context.SubStatus)
                        {
                            case ContextSubStatus.Processed:
                                Context.AddProcessedFiles(fullFilePath);
                                break;
                        }
                        break;
                    case ContextStatus.Fail:
                        Context.AddFailedFiles(fullFilePath);
                        break;
                }
                if (Directory.Exists(tmpFolderPath))
                {
                    Directory.Delete(tmpFolderPath, true);
                }
                if (!string.IsNullOrEmpty(tmpFilePath) && File.Exists(tmpFilePath))
                {
                    File.Delete(tmpFilePath);
                }
                Logger.Info($"Finish processing: {filePath}");
            }
        }

        protected virtual void PreProcess(string sourceFolderPath)
        {
        }

        protected virtual void PostProcess(string sourceFolderPath, string destFilePath)
        {
        }
    }
}
