﻿using OfficeStringReplacerCore.Model;
using NLog;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class Processer
    {
        protected Logger Logger { get; set; }

        protected Context Context { get; }

        protected ProcesserParam ProcesserParam { get; }

        protected IValueProcesser ValueProcesser { get; set; }

        public Processer(Context context, ProcesserParam processerParam)
        {
            Context = context;
            ProcesserParam = processerParam;
            Logger = LogManager.GetLogger(GetType().Name);
        }

        public void ThrowIfCancelRequested()
        {
            if (Context.IsCancelRequested)
            {
                Context.Status = ContextStatus.Canceled;
                throw new CancelException();
            }
        }
    }
}
