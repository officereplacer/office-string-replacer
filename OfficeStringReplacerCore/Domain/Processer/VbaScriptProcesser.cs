﻿using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Infrastructure;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Util;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public abstract class VbaScriptProcesser : Processer
    {
        public VbaScriptProcesser(Context context, ProcesserParam param) : base(context, param)
        { }

        public abstract void Process(string file);

        protected void Process(VBProject project)
        {
            Logger.Info($"Start processing VBProject");
            try
            {
                using (var vbComponents = new DisposableCom<VBComponents>(project.VBComponents))
                {
                    foreach (var component in Utils.EnamerateAsDisposableCom(vbComponents.Value))
                    {
                        using (component)
                        using (var codeModule = new DisposableCom<CodeModule>(component.Value.CodeModule))
                        {
                            var name = component.Value.Name;
                            Logger.Debug($"Component name: {name}");
                            if (component.Value.Type == vbext_ComponentType.vbext_ct_StdModule)
                            {
                                ValueProcesser.Process(Context, component.Value, nameof(component.Value.Name));
                                if (Context.SkipRest)
                                {
                                    return;
                                }
                            }
                            ValueProcesser.Process(Context, codeModule.Value);
                            if (Context.SkipRest)
                            {
                                return;
                            }
                        }
                    }
                }
            }
            finally
            {
                Logger.Info($"Finish processing VBProject");
            }
        }

        protected void Process(VBProjects projects)
        {
            Logger.Info($"Start processing VBProjects");
            try
            {
                foreach (var project in Utils.EnamerateAsDisposableCom(projects))
                {
                    using (project)
                    {
                        Process(project.Value);
                    }
                }
            }
            finally
            {
                Logger.Info($"Finish processing VBProjects");
            }
        }
    }
}