﻿using Microsoft.Office.Interop.Word;
using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;
using System;
using System.IO;
using System.Threading;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public class WordVbaScriptProcesser : VbaScriptProcesser
    {
        public WordVbaScriptProcesser(Context context, ProcesserParam param) : base(context, param)
        { }

        public override void Process(string file)
        {
            var fullPath = Path.GetFullPath(file);
            using (var wordApp = new DisposableCom<Microsoft.Office.Interop.Word.Application>(
                new Microsoft.Office.Interop.Word.Application() { 
                    DisplayAlerts = WdAlertLevel.wdAlertsMessageBox
                },
                (_) => { _.Quit(); }))
            using (var documents = new DisposableCom<Documents>(wordApp.Value.Documents))
            using (var document = new DisposableCom<Document>(
                documents.Value.Open(file),
                (_) =>
                {
                    _.Save();
                    //Wait a while in order to finish saving
                    Thread.Sleep(10);
                    _.Close(true);
                }))
            using (var vbe = new DisposableCom<VBE>(wordApp.Value.VBE))
            using (var projects = new DisposableCom<VBProjects>(vbe.Value.VBProjects))
            {
                Process(projects.Value);
            }
            //Force gabage collecting for the Application object
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
