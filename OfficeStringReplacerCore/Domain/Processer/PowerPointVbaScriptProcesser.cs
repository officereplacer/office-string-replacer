﻿using Microsoft.Office.Interop.PowerPoint;
using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;
using System;
using System.IO;
using System.Threading;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public class PowerPointVbaScriptProcesser : VbaScriptProcesser
    {
        public PowerPointVbaScriptProcesser(Context context, ProcesserParam param) : base(context, param)
        { }

        public override void Process(string file)
        {
            var fullPath = Path.GetFullPath(file);
            using (var powerpointApp = new DisposableCom<Microsoft.Office.Interop.PowerPoint.Application>(
                new Microsoft.Office.Interop.PowerPoint.Application(),
                (_) => { _.Quit(); }))
            using (var presentations = new DisposableCom<Presentations>(powerpointApp.Value.Presentations))
            using (var presentation = new DisposableCom<Presentation>(
                presentations.Value.Open(fullPath, WithWindow: Microsoft.Office.Core.MsoTriState.msoFalse),
                (_) =>
                {
                    _.Save();
                    //Wait a while in order to finish saving
                    Thread.Sleep(10);
                    _.Close();
                }))
            using (var project = new DisposableCom<VBProject>(presentation.Value.VBProject))
            {
                Process(project.Value);
            }
            //Force gabage collecting for the Application object
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
