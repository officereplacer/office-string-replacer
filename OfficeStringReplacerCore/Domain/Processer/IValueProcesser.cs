﻿using Microsoft.Office.Interop.Access;
using Microsoft.Office.Interop.Access.Dao;
using Microsoft.Vbe.Interop;
using OfficeStringReplacerCore.Model;

namespace OfficeStringReplacerCore.Domain.Processer
{
    public interface IValueProcesser
    {
        void Process(Context context, Recordset recordset, Field field);

        void Process(Context context, object obj, string propertyName);

        void ProcessTables(Context context, Database db);

        void Process(Context context, CodeModule codeModule);

        void Process(Context context, DoCmd doCmd, AcObjectType acObjectType, string name);
    }
}
