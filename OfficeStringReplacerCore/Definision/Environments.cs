﻿using System;
using System.IO;

namespace OfficeStringReplacerCore.Definision
{
    public class Environments
    {
        public static string AppName { get; set; }

        public static bool EvaluationMode =>
#if EVALUATION_MODE
            true;
#else
            false;
#endif
    }
}
