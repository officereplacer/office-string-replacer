﻿namespace OfficeStringReplacerCore.Definision
{
    public class Consts
    {
        public const string ReplaceFailedFileName = "replace-failed-files.txt";
        public const string SearchFailedFileName = "search-failed-files.txt";
        public const string CurrentLogFileName = "OsrLog.log";
        public const string ConfigFileName = "config.json";
        public const string BackupFolderBase = "osr.backup";
        public const uint DsnNotFoundErrorCode = 0x800A0E29;
    }
}
