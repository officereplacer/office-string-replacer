@REM Defines the certificate to use for signatures.
@REM Use Powershell to find the available certs:
@REM
@REM PS> Get-ChildItem -Path Cert:CurrentUser\My
@REM
set cert=73E7B9D1F72EDA033E7A9D6B17BC37A96CE8513A
set timestamp=http://timestamp.sectigo.com

@REM ==================
@REM Compile C# sources
@REM ==================
msbuild /p:Configuration=Release /p:Platform=x64 /t:Rebuild
msbuild /p:Configuration=Release /p:Platform=x86 /t:Rebuild
msbuild /p:Configuration=ReleaseForEvaluation /p:Platform=x64 /t:Rebuild
msbuild /p:Configuration=ReleaseForEvaluation /p:Platform=x86 /t:Rebuild

@REM ===========================
@REM Sign the exec and dll files
@REM ===========================
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% OfficeStringReplacer\bin\x64\Release\OfficeStringReplacer*.exe
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% ShortcutStringReplacer\bin\x64\Release\ShortcutStringReplacer*.exe
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% OfficeStringReplacer\bin\x86\Release\OfficeStringReplacer*.exe
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% ShortcutStringReplacer\bin\x86\Release\ShortcutStringReplacer*.exe

signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% OfficeStringReplacer\bin\x64\Release\OfficeStringReplacerCore*.dll
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% ShortcutStringReplacer\bin\x64\Release\OfficeStringReplacerCore*.dll
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% OfficeStringReplacer\bin\x86\Release\OfficeStringReplacerCore*.dll
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% ShortcutStringReplacer\bin\x86\Release\OfficeStringReplacerCore*.dll

signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% OfficeStringReplacer\bin\x64\ReleaseForEvaluation\OfficeStringReplacer*.exe
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% OfficeStringReplacer\bin\x86\ReleaseForEvaluation\OfficeStringReplacer*.exe
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% OfficeStringReplacer\bin\x64\ReleaseForEvaluation\OfficeStringReplacerCore*.dll
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% OfficeStringReplacer\bin\x86\ReleaseForEvaluation\OfficeStringReplacerCore*.dll

pushd OfficeStringReplacer
  @REM ================
  @REM Build installers
  @REM ================

  iscc.exe /O..\dest OfficeStringReplacer-x64-Office.iss
  iscc.exe /O..\dest OfficeStringReplacer-x86-Office.iss

  @REM ===============================
  @REM Build installers for evaluation
  @REM ===============================

  iscc.exe /O..\dest OfficeStringReplacerEvaluation-x64-Office.iss
  iscc.exe /O..\dest OfficeStringReplacerEvaluation-x86-Office.iss
popd

pushd ShortcutStringReplacer
  @REM ================
  @REM Build installers
  @REM ================
  iscc.exe /O..\dest ShortcutStringReplacer-x64.iss
  iscc.exe /O..\dest ShortcutStringReplacer-x86.iss
popd

@REM ===================
@REM Sign the installers
@REM ===================
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% dest\OfficeStringReplacer*.exe
signtool sign /t %timestamp% /fd SHA256 /sha1 %cert% dest\ShortcutStringReplacer*.exe

