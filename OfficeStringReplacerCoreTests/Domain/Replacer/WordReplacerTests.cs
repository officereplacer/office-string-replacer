﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Domain.Replacer;
using OfficeStringReplacerTests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Domain.Replacer.Tests
{
    [TestClass()]
    public class WordReplacerTest
    {
        public TestContext TestContext { get; set; }

        public static string InFolder = @"..\..\..\TestFile\Word\In";
        public static string OutFolder = @"..\..\..\TestFile\Word\Out";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            try
            {
                Directory.Delete(OutFolder, true);
            }
            catch
            {
            }
            Directory.CreateDirectory(OutFolder);
        }


        [TestMethod()]
        [TestCase("test1.docx", "テストライン", "テストライン_置換後")]
        [TestCase("test2.doc", "テストライン", "テストライン_置換後")]
        [TestCase("test3.docm", "テストコメント", "テストコメント_置換後")]
        [TestCase("test4.docx", "image", "image_置換後")]
        [TestCase("test5.docx", @"test1.docx", @"test4.docx")]
        [TestCase("test6.docx", @"In\test1.xlsx", "Out\test1.xlsx")]
        public void ReplaceTest()
        {
            TestContext.Run((string targetFile, string from, string to) =>
            {
                var originalTestfile = Path.Combine(InFolder, targetFile);
                var testFile = Path.GetFullPath(Path.Combine(OutFolder, targetFile));
                File.Copy(originalTestfile, testFile);
                var param = new ProcesserParam()
                {
                    TargetPath = testFile,
                    ValueProcesserParam = new ValueProcesserParam()
                    {
                        From = from,
                        To = to,
                        IgnoreCase = true
                    }
                };
                var context = new Context();
                var wordReplacer = new WordReplacer(context, param);
                wordReplacer.Process();
                context.ProcessedFiles.Contains(testFile).IsTrue(targetFile);
            });
        }
    }
}