﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Domain.Replacer;
using OfficeStringReplacerTests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Domain.Replacer.Tests
{
    [TestClass()]
    public class PowerPointReplacerTest
    {
        public TestContext TestContext { get; set; }

        public static string InFolder = @"..\..\..\TestFile\PowerPoint\In";
        public static string OutFolder = @"..\..\..\TestFile\PowerPoint\Out";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            try
            {
                Directory.Delete(OutFolder, true);
            }
            catch
            {
            }
            Directory.CreateDirectory(OutFolder);
        }


        [TestMethod()]
        [TestCase("test1.pptx", "タイトル", "タイトル_置換後")]
        [TestCase("test2.ppt", "タイトル", "タイトル_置換後")]
        [TestCase("test3.pptm", "テストコメント", "テストコメント_置換後")]
        [TestCase("test4.ppt", "テストコメント", "テストコメント_置換後")]
        [TestCase("test5.pptx", @"image", @"image_置換後")]
        public void ReplaceTest()
        {
            TestContext.Run((string targetFile, string from, string to) =>
            {
                var originalTestfile = Path.Combine(InFolder, targetFile);
                var testFile = Path.GetFullPath(Path.Combine(OutFolder, targetFile));
                File.Copy(originalTestfile, testFile);
                var param = new ProcesserParam()
                {
                    TargetPath = testFile,
                    ValueProcesserParam = new ValueProcesserParam()
                    {
                        From = from,
                        To = to,
                        IgnoreCase = true
                    }
                };
                var context = new Context();
                var powerPointReplacer = new PowerPointReplacer(context, param);
                powerPointReplacer.Process();
                context.ProcessedFiles.Contains(testFile).IsTrue(targetFile);
            });
        }
    }
}