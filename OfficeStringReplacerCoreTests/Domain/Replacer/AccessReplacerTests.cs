﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Domain.Replacer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Domain.Replacer.Tests
{
    [TestClass()]
    public class AccessReplacerTest
    {
        public TestContext TestContext { get; set; }

        public static string InFolder = @"..\..\..\TestFile\Access\In";
        public static string OutFolder = @"..\..\..\TestFile\Access\Out";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            try
            {
                Directory.Delete(OutFolder, true);
            }
            catch
            {
            }
            Directory.CreateDirectory(OutFolder);
        }

        [TestMethod()]
        [TestCase("test1.accdb", "テキスト", "テキスト_置換後")]
        //This test depends Out\test1.accdb, ensure Out\test1.accdb existent.
        [TestCase("test2.accdb", @"Access\In\test1.accdb", @"Access\Out\test1.accdb")]
        [TestCase("test3_macro.accdb", "マクロテスト", "マクロテスト_置換後")]
        [TestCase("test4.mdb", "テキスト", "テキスト_置換後")]
        [TestCase("test5.mdb", @"Access\In\test4.mdb", @"Access\Out\test4.mdb")]
        [TestCase("test6_macro.mdb", "マクロテスト", "マクロテスト_置換後")]
        [TestCase("test7_query.accdb", "テーブル1", "テーブル1_置換後")]
        [TestCase("test8_report.accdb", "テスト値", "テスト値_置換後")]
        [TestCase("test9.accdb", @"テーブル1", @"テーブル1-2")]
        [TestCase("test10.accdb", @"In\LinkedCsv", @"In\LinkedCsv2")]
        [TestCase("test11.accdb", @"LinkedCsv.csv", @"LinkedCsv2.csv")]
        public void ReplaceTest()
        {
            TestContext.Run((string targetFile, string from, string to) =>
            {
                var originalTestfile = Path.Combine(InFolder, targetFile);
                var testFile = Path.GetFullPath(Path.Combine(OutFolder, targetFile));
                File.Copy(originalTestfile, testFile);
                var param = new ProcesserParam()
                {
                    TargetPath = testFile,
                    ValueProcesserParam = new ValueProcesserParam()
                    {
                        From = from,
                        To = to,
                        IgnoreCase = true
                    }
                };
                var context = new Context();
                var excelReplacer = new AccessReplacer(context, param);
                excelReplacer.Process(testFile);
                context.ProcessedFiles.Contains(testFile).IsTrue(targetFile);
            });
        }
    }
}