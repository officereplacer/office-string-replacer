﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeStringReplacerCore.Model;
using OfficeStringReplacerCore.Domain.Searcher;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Domain.Searcher.Tests
{
    [TestClass()]
    public class AccessVbaScriptSearcherTests
    {
        public static string InFolder = @"..\..\TestFile\Access\In";
        public static string OutFolder = @"..\..\TestFile\Access\Out";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            try
            {
                Directory.Delete(OutFolder, true);
            }
            catch
            {
            }
            Directory.CreateDirectory(OutFolder);
        }

        [TestMethod()]
        public void ProcessTest()
        {
            var originalTestfile = Path.Combine(InFolder, "test3_macro.accdb");
            var testFile = Path.Combine(OutFolder, "test3_macro.accdb");
            File.Copy(originalTestfile, testFile);
            var param = new ProcesserParam()
            {
                TargetPath = testFile,
                ValueProcesserParam = new ValueProcesserParam()
                {
                    From = "テキスト",
                }
            };
            var accessReplacer = new AccessVbaScriptSearcher(new Context(), param);
            accessReplacer.Process(testFile);
        }
    }
}