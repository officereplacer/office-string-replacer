﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OfficeStringReplacerCore.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeStringReplacerCore.Util.Tests
{
    [TestClass()]
    public class FileBackupperTests
    {
        public static string InFolder = @"..\..\..\TestFile\FileBackupper\In";
        public static string OutFolder = @"..\..\..\TestFile\FileBackupper\Out";

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            try
            {
                Directory.Delete(OutFolder, true);
            }
            catch
            {
            }
            Directory.CreateDirectory(OutFolder);
        }

        [TestMethod()]
        public void BackupDictoryTest()
        {
            var outFolderFullPath = Path.GetFullPath(OutFolder);
            var inFolderFullPath = Path.GetFullPath(InFolder);
            var targetFile = Path.Combine(inFolderFullPath, "testfile1.txt");
            var fileBackupper = new FileBackupper(outFolderFullPath, true);
            fileBackupper.Backup(targetFile);

            var root = Path.GetPathRoot(targetFile);
            var pathFromRoot = root.Length > 0 ?
                targetFile.Substring(root.Length) :
                targetFile;
            var expectedPath = Path.Combine(outFolderFullPath, fileBackupper.BackupDirectoryName, pathFromRoot);
            File.Exists(expectedPath).IsTrue();
        }

        [TestMethod()]
        public void BackupDirectTest()
        {
            var outFolderFullPath = Path.GetFullPath(OutFolder);
            var inFolderFullPath = Path.GetFullPath(InFolder);
            var targetFile = Path.Combine(inFolderFullPath, "testfile1.txt");
            var fileBackupper = new FileBackupper(outFolderFullPath, false);
            fileBackupper.Backup(targetFile);
            var expectedPath = Path.Combine(outFolderFullPath, fileBackupper.BackupDirectoryName, "testfile1.txt");
            File.Exists(expectedPath).IsTrue();
        }
    }
}